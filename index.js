const { Maybe, Some, None, Nothing, Either, IO } = require("monet");

const {
  curry,
  compose,
  map,
  prop,
  append,
  add,
  toString,
  chain,
} = require("ramda");
const moment = require("moment");
const { studyTask } = require("./src/studyTask");
const { fantasyLandDaggy } = require("./src/fantas");
const { allonge } = require("./src/allonge");

/* const getField = attr => obj => obj[attr]
const produceResults = weatherObj => Maybe.of(weatherObj).map(getField("date")).map(a => console.log(a))
const getDog = () => fetch('https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY').then(x => x.json()).then(x => produceResults(x)).catch(e => console.log(e))
getDog()
http://learnyouahaskell.com/functors-applicative-functors-and-monoids
*/

/* const add = a => b => a + b;
const add2 = val => val === 1 ? Maybe.of(3) : None()
var result = Some(1).ap(Some(2).map(add))

console.log(result); */

const withdraw = curry((amount, { balance }) =>
  balance >= amount
    ? Maybe.some({ balance: balance - amount })
    : Maybe.fromNull(null)
);

const updateLedger = (account) => account;

const remainingBalance = ({ balance }) => `Your balance is $${balance}`;

const finishTransaction = compose(remainingBalance, updateLedger);
const checkForBroke = (x) => (x.isSome() ? x.toArray()[0] : "You are broke");
const getTwenty = compose(checkForBroke, map(finishTransaction), withdraw(20));

/* console.log(getTwenty({ balance: 200 }))
console.log(getTwenty({ balance: 10 })) */
let val = Either.of("rain").map((str) => `b${str}`);
/* console.log(val); */
let val2 = Either.left("rain").map(
  (str) => `It's going ${str}, better bring your umbrella`
);
/* console.log(val2); */
let val3 = Either.of({ host: "localhost", port: 80 }).map(prop("host"));
/* console.log(val3); */
let val4 = Either.left("rolls eyes....").map(prop("host"));

const getAge = curry((now, user) => {
  const birthDate = moment(user.birthDate, "YYYY-MM-DD");

  return birthDate.isValid()
    ? Either.of(now.diff(birthDate, "years"))
    : Either.left("Birth date count not be parsed");
});

const forture = compose(
  append("If you survive, you will be"),
  toString,
  add(1)
);

const zoltar = compose(map(forture), getAge(moment()));
/* const obj = {
    value: "Somevalue"
}
const obj2 = {
    value: ""
}

const read = s => IO(() => s.value)
const write = s => val => IO(() => s.value = val)
const toUpper = text => text.toUpperCase();
const changeToUpperId = read(obj).map(toUpper).flatMap(write(obj2))
changeToUpperId.run();
const user = {
    name: "nick"
}
const validateUser = curry((validate, user) => validate(user).map(_ => user))
const validateName = ({ name }) => (name.length > 3 ? Either.of(null) : Either.left("User name not long enough"))
const stringIo = s => IO(() => s)
const register = compose(stringIo, validateUser(validateName))
register(user).map(x => console.log(x)).run() */

/* studyTask(); */
fantasyLandDaggy();
/* allonge(); */
