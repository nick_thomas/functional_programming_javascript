const daggy = require("daggy");

function fantasyLandDaggy() {
  const Coord = daggy.tagged("Coord", ["x", "y", "z"]);
  const Line = daggy.tagged("Coord", ["from", "to"]);

  Coord.prototype.equals = function coordEquals(that) {
    return this.x == that.x && this.y == that.y && this.z == that.z;
  };
  Coord.prototype.lte = function coordLte(that) {
    return (
      this.x < that.x ||
      (this.x == that.x && this.y < that.y) ||
      (this.x == that.x && this.y == that.y && this.z < that.z) ||
      this.equals(that)
    );
  };

  Line.prototype.equals = function lineEquals(that) {
    return this.from.equals(that.from) && this.to.equals(that.to);
  };
  Line.prototype.lte = function lineLte(that) {
    return (
      lt(this.from, that.from) ||
      (this.from.equals(that.from) && lt(this.to, that.to)) ||
      this.equals(that)
    );
  };
  const gt = function (x, y) {
    return !lte(x, y);
  };

  // Greater than or equal.
  // gte :: Ord a => a -> a -> Boolean
  const gte = function (x, y) {
    return gt(x, y) || x.equals(y);
  };

  // Less than. The OPPOSITE of gte!
  // lt :: Ord a => a -> a -> Boolean
  const lt = function (x, y) {
    return !gte(x, y);
  };

  // And we already have lte!
  // lte :: Ord a => a -> a -> Boolean
  const lte = function (x, y) {
    return x.lte(y);
  };
  const copy = (xs) => [...xs];

  //- Sort a list of Ords using bubble sort algorithm
  // bubbleSort :: Ord a => [a] -> [a]
  const bubbleSort = (xs_) => {
    const xs = copy(xs_);
    let n = xs.length - 1;
    let swapped = true;
    while (swapped) {
      swapped = false;
      for (let i = 1; i <= n; i++) {
        const curr = xs[i];
        const prev = xs[i - 1];
        if (gt(prev, curr)) {
          xs[i - 1] = curr;
          xs[i] = prev;
          swapped = true;
        }
      }
    }
    return xs;
  };

  const merge = (arr1, arr2) => {
    let finalArray = [];
    let i = 0;
    let j = 0;
    while (i < arr1.length && j < arr2.length) {
      if (lt(arr1[i], arr2[j])) {
        finalArray.push(arr1[i]);
        i++;
      } else {
        finalArray.push(arr2[i]);
        j++;
      }
    }
    let newArray = [];
    if (i !== arr1.length) {
      newArray.push(...arr1.slice(i, arr1.length));
    } else if (j !== arr2.length) {
      newArray.push(...arr2.slice(j, arr2.length));
    }
    return finalArray.concat(...newArray);
  };

  const mergeSort = (xs) => {
    if (xs.length <= 1) return xs;
    let mid = Math.floor(xs.length / 2);
    let left = mergeSort(xs.slice(0, mid));
    let right = mergeSort(xs.slice(mid));
    return merge(left, right);
  };
  const quickSort = (arr, low, high) => {
    if (low < high) {
      let pi = partition(arr, low, high);
      quickSort(arr, low, pi - 1);
      quickSort(arr, pi + 1, high);
      return arr;
    }
  };
  const partition = (arr, low, high) => {
    let pivot = arr[high];
    let i = low - 1;
    for (let j = low; j <= high - 1; j++) {
      if (lt(arr[j], pivot)) {
        i++;
        swap(arr, i, j);
      }
    }
    swap(arr, i + 1, high);
    return i + 1;
  };
  const swap = (arr, i, j) => {
    let temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
  };
  const coord_list = [Coord(1, 8, 3), Coord(1, 2, 1), Coord(1, 1, 2)];
  let origin = Coord(0, 0, 0);
  const line_list = [
    Line(origin, Coord(1, 2, 3)),
    Line(Coord(1, 2, 1), Coord(1, 2, 2)),
    Line(origin, Coord(1, 1, 2)),
  ];

  console.log(quickSort(line_list, 0, line_list.length - 1));
}
// Uncomment to execute
// exports.fantasyLandDaggy = fantasyLandDaggy;

function semiGroup() {
  const Sum = daggy.tagged("Sum", ["val"]);
  Sum.prototype.concat = function (that) {
    return Sum(this.val + that.val);
  };
  const Product = daggy.tagged("Product", ["val"]);
  Product.prototype.concat = function (that) {
    return Product(this.val * that.val);
  };

  const Max = daggy.tagged("Max", ["val"]);
  Max.prototype.concat = function (that) {
    return Max(Math.max(this.val, that.val));
  };

  const Min = daggy.tagged("Min", ["val"]);
  Min.prototype.concat = function (that) {
    return Min(Math.min(this.val, that.val));
  };

  console.log(Sum(2).concat(Sum(3)).val);
  console.log(Product(2).concat(Product(3)).val);
  console.log(Max(2).concat(Max(3)).val);
  console.log(Min(2).concat(Min(3)).val);

  const Any = daggy.tagged("Any", ["val"]);
  Any.prototype.concat = function (that) {
    return Any(this.val || that.val);
  };

  const All = daggy.tagged("All", ["val"]);
  All.prototype.concat = function (that) {
    return All(this.val && that.val);
  };
  const Tuple = daggy.tagged("Tuple", ["a", "b"]);

  // concat :: (Semigroup a, Semigroup b) =>
  // Tuple a b -> Tuple a b -> Tuple a b
  Tuple.prototype.concat = function (that) {
    return Tuple(this.a.concat(that.a), this.b.concat(that.b));
  };

  console.log(Tuple(Sum(1), Any(false)).concat(Tuple(Sum(2), Any(true))));

  Sum.empty = () => Sum(0);
  const fold = (M) => (xs) =>
    xs.reduce((acc, x) => acc.concat(M(x)), M.empty());

  console.log(fold(Sum)([1, 2, 3, 4, 5]).val);
}

function contraVariant() {
  const Predicate = daggy.tagged("Predicate", ["f"]);

  // Make a Predicate that runs 'f' to get from
  // 'b' to 'a', then uses the original
  // predicate function!
  // contramap:: Predicate a-> (b->a)
  // -> Predicate b
  Predicate.prototype.contramap = function (f) {
    return Predicate((x) => this.f(f(x)));
  };
  // isEven :: Predicate Int
  const isEven = Predicate((x) => x % 2 === 0);

  //Take a string, run .length, then isEven.
  // lengthIsEven :: Predicate String
  const lengthIsEven = isEven.contramap((x) => x.length);

  // type ToString = a :: a->String
  const ToString = daggy.tagged("ToString", ["f"]);

  // Add a pre-processor to pipeline
  ToString.prototype.contramap = function (f) {
    return ToString((x) => this.f(f(x)));
  };

  // Convert an int to a string
  // intToString :: ToString Int
  const intToString = ToString((x) => "int(" + x + ")").contramap((x) => x | 0);

  // Convert an array of strings to a string
  // stringArrayToString :: ToString[String]
  const stringArrayToString = ToString((x) => "[" + x + "]").contramap((x) =>
    x.join(", ")
  );

  //Given a To String instance for a type, convert an array of a type to a string.
  // arrayToString :: ToString a -> ToString[a]
  const arrayToString = (t) => stringArrayToString.contramap((x) => x.map(t.f));

  // Convert an integer array to a string.
  // intsToString :: ToString [Int]
  const intsToString = arrayToString(intToString);

  // Aaand they compose! 2D int array.
  // matrixToString :: ToString[[Int]]
  const matrixToString = arrayToString(intsToString);

  console.log(matrixToString.f([[1, 3, 4]]));
}

function applyCheck() {
  const lift2 = (f) => (a) => (b) => b.ap(a.map(f));

  const Identity = daggy.tagged("Identity", ["x"]);

  Identity.prototype.map = function (f) {
    return new Identity(f(this.x));
  };

  Identity.prototype.ap = function (b) {
    return new Identity(b.x(this.x));
  };

  const value = lift2((x) => (y) => x + y)(Identity(2))(Identity(3));

  console.log(value);

  Array.prototype.ap = function(fs) {
    return [].concat(...fs.map(
      f=>this.map(f)
    ))
  }

  console.log([2,3,4].ap([]))
  console.log([2,3,4].ap([x=>x+`!`]))
  console.log([2,3,4].ap([x=>x+`!`, x=>x+ `?`]))

}

function alternative() {
  const Alt = daggy.tagged('Alt',['value'])

  // Alt is a valid semigroup!
  Alt.prototype.concat = function(that) {
    return Alt(this.value.alt(that.value))
  }

  // The value MUST be a Plus-implementer.
  // And, as usual we need a TypeRep...
  const Plus = T => {
    const Plus_ = daggy.tagged('Plus',['value'])

    // Plus is a valid semigroup
    Plus_.prototype.concat =
      function (that) {
        return Plus(
        this.value.alt(
        that.value))
      }


  //... and a valid monoid!
  Plus_.empty = () => Plus_(T.zero())
  }
}

function coMonad() {
  const Pair = daggy.tagged('Pair',['_1','_2']);

  const Store = daggy.tagged('Stored',['lookup','pointer']);

  Array.prototype.equals = function(that) {
    return (
      this.length === that.length &&
      this.every((x,i)=> (x.equals ? x.equals(that[i]) : x === that[i]))
    );
  };

  Function.prototype.map = function(f) {
    return x => f(this(x));
  };

  Store.prototype.peek = function(p) {
    return this.seek(p).extract();
  };

  Store.prototype.seek = function(p) {
    return Store(this.lookup,p);
  }

  Store.prototype.map = function(f) {
    return Store(this.lookup.map(f),this.pointer);
  }

  Store.prototype.extend = function(f) {
    const {lookup, pointer} = this;
    return Store(p=> f(Store(lookup,p)),pointer);
  };

  Store.prototype.extract = function() {
    return this.lookup(this.pointer)
  }

  // Game of Life //

  let start = [
    [true,true,false,false],
    [true,false,true,false],
    [true,false,false,true],
    [true,false,true,false]
  ];

  const Game = Store(
    ({_1: x, _2:y}) => (y in start && x in start[y] ? start[y][x] : false),
    Pair(0,0)
  );

  // What will the current cell be next time?
  // isSurvivor::Store(Pair Int Int) Bool
  // -> Bool
  const isSurvivor = store =>{
    const {_1:x,_2:y} = store.pointer;
    const neighbours = [
      Pair(x-1,y-1),
      Pair(x,y-1),
      Pair(x+1,y-1),
      Pair(x-1,y),
      Pair(x+1,y),
      Pair(x-1,y+1),
      Pair(x,y+1),
      Pair(x+1,y+1)
    ].map(x=>store.peek(x)).filter(x=>x).length

    return(
      neighbours === 3 ||
      (store.extract() && neighbours === 2)
    );
  };

  // Impurities

  const loop = setInterval(()=>{
    // Get the next step
    const next = Game.extend(isSurvivor);

    // Extract the whole result
    const result = start.map((row,y)=>
      row.map((column,x)=> next.lookup(Pair(x,y)))
    );

    // Stop looping if we freeze.
    if (start.equals(result)) clearInterval(loop);

    // Pretty print the result.
    console.log(result.map(row=>row.map(column=>column ? "X" : "")));

    start = result;
    console.log();
  },100);
}
exports.fantasyLandDaggy = coMonad;
