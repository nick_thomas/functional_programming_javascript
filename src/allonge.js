const EMPTY = {};
const copyme = {
    first: 1,
    rest: {
        first: 2,
        rest: {
            first: 3,
            rest: {
                first: 4,
                rest: {
                    first: 5,
                    rest: EMPTY
                }
            }
        }
    }
};
const copy = (node, head = null, tail = null) => {
    if (node === EMPTY) {
        return head;
    }
    else if (tail === null) {
        const { first, rest } = node;
        const newNode = { first, rest };
        return copy(rest, newNode, newNode);
    }
    else {
        const { first, rest } = node;
        const newNode = { first, rest };
        tail.rest = newNode;
        return copy(node.rest, head, newNode);
    }
}
function allonge() {
    console.log(copy(copyme))
}
exports.allonge = allonge