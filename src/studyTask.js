const folktale = require("folktale");
const { rejected, task, of } = require("folktale/concurrency/task");
const Task = require("folktale/concurrency/task/_task");
const Maybe = require("folktale/maybe");
const fs = require("fs");
const {
  prop,
  identity,
  curry,
  compose,
  map,
  join,
  take,
  split,
  sequence,
  traverse,
  ap,
  multiply,
  add,
} = require("ramda");

const readFile = (filename) =>
  task((resolver) => {
    fs.readFile(filename, "utf-8", (err, data) => {
      return err ? rejected(err) : resolver.resolve(data);
    });
  });

const someValue = {
  something: {
    else: {
      value: 42,
    },
  },
};

const value = Maybe.Just(someValue)
  .map(prop("something"))
  .map(prop("else"))
  .map(prop("value"))
  .chain(identity);

const safeProp = curry((x, obj) => Maybe.of(obj[x]));
const safeHead = safeProp(0);
const firstAddressStreet = compose(
  map(map(safeProp("street"))),
  map(safeHead),
  safeProp("addresses")
);

// chapter 12
const extract = (x) => x.future().run();
const firstWords = compose(join(" "), take(3), split(" "));

const tildr = compose(map(firstWords), readFile);

const studyTask = () => {
  readFile(__dirname + "/assets/sample.txt")
    .run()
    .future()
    .map((val) => val);
  /* console.log(value); */
  const output = firstAddressStreet({
    addresses: [
      { street: { name: "Mulburry", number: 8402 }, postcode: "WC2N" },
    ],
  });
  /*const value = traverse(of, tildr, [__dirname + '/assets/sample.txt', __dirname + '/assets/sample2.txt'])
     value.run().future().map(val => console.log(val))
     https://www.samhh.com/blog/js-fp-jargon/ */
  let val1 = ap([multiply(2), add(3)], [1, 2, 3]);
  console.log(val1);
};

exports.studyTask = studyTask;
